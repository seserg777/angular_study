import { Component } from '@angular/core';
import { TestService } from '../common/services/test.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  /**
   * Simple string property, assigned right from the start
   * @type { String }
   */
  public simpleStringProperty: string = 'Simple String Property';

  /**
   * Simple string property, will be assigned after 5 sec of awaiting
   * @type { String }
   */
  public asyncSimpleStringProperty: string;

  /**
   * Strings from 'zero' to 'nine'. We'll show them via the *ngFor loop
   * @type { String[] }
   */
  public arrayOfString: string[] = [];

  /**
   * Value to show how "json" pipe works
   * @type { Object }
   */
  public objectValue: { [ key: string ]: number } = { a: 1, b: 2, c: 3 };

  /**
   * Date object to show how "date" pipe works
   * @type { Date }
   */
  public date: Date = new Date();

  /**
   * Styles to change the attributes of HTML elements
   * @type { Object }
   */
  public colors: { color1: string, color2: string, color3: string } = {
    color1: '#000',
    color2: '#f00',
    color3: '#00ff03'
  };

  /**
   * Will be true when user holds the button within the DIV.mouseEvents
   * @type { Boolean }
   */
  public mouseHoldState: boolean;

  constructor(private testService: TestService) {
    setTimeout(// Displaying this string after 5 sec of awaiting
      (): void => {
        this.asyncSimpleStringProperty = 'Async simple string property';
      },
      5000
    );

    setInterval(// Changing array length with added values every 2 sec
      (): void => {
        switch (this.arrayOfString.length) {
          case 0:
            this.arrayOfString.push('Zero');
            break;
          case 1:
            this.arrayOfString.push('One');
            break;
          case 2:
            this.arrayOfString.push('Two');
            break;
          case 3:
            this.arrayOfString.push('Three');
            break;
          case 4:
            this.arrayOfString.push('Four');
            break;
          case 5:
            this.arrayOfString.push('Five');
            break;
          case 6:
            this.arrayOfString.push('Six');
            break;
          case 7:
            this.arrayOfString.push('Seven');
            break;
          case 8:
            this.arrayOfString.push('Eight');
            break;
          case 9:
            this.arrayOfString.push('Nine');
            break;
          default:
            this.arrayOfString = [];
        }
      },
      2000
    );

    setInterval(// Changing colors every 5 sec
      (): void => {
        this.colors = {
          color1: this.colors.color2,
          color2: this.colors.color3,
          color3: this.colors.color1
        };
      },
      5000
    );
  }

  /**
   * Getter for the testService property.
   * Services shouldn't be public and requested from HTML template
   * @returns { String }
   */
  public get serviceValue(): string {
    return this.testService.testProperty;
  }

  /**
   * Handler for the mousedown event for the button within DIV.mouseDownHandler element
   * @param event { MouseEvent }
   * @returns { void }
   */
  public mouseDownHandler(event: MouseEvent): void {
    console.log('Clicked', event);
    this.mouseHoldState = true;
  }

  /**
   * Handler for the mouseup event for the button within DIV.mouseDownHandler element
   * @param event { MouseEvent }
   * @returns { void }
   */
  public mouseUpHandler(event: MouseEvent): void {
    console.log('Released', event);
    this.mouseHoldState = false;
  }
}
