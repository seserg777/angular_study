import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })

export class TestService {
  /**
   * Some random value to return
   * @type { String }
   */
  public testProperty: string = 'Test value from the service';
}
